package ru.ovechkin.tm.bootstrap;
//Главная петля приложения

import ru.ovechkin.tm.api.controller.ICommandController;
import ru.ovechkin.tm.api.controller.IProjectController;
import ru.ovechkin.tm.api.controller.ITaskController;
import ru.ovechkin.tm.api.repository.IProjectRepository;
import ru.ovechkin.tm.api.repository.ITaskRepository;
import ru.ovechkin.tm.api.service.ICommandService;
import ru.ovechkin.tm.api.service.IProjectService;
import ru.ovechkin.tm.api.service.ITaskService;
import ru.ovechkin.tm.constant.IArgumentConst;
import ru.ovechkin.tm.constant.ICmdConst;
import ru.ovechkin.tm.controller.CommandController;
import ru.ovechkin.tm.controller.ProjectController;
import ru.ovechkin.tm.controller.TaskController;
import ru.ovechkin.tm.repository.CommandRepository;
import ru.ovechkin.tm.repository.ProjectRepository;
import ru.ovechkin.tm.repository.TaskRepository;
import ru.ovechkin.tm.service.CommandService;
import ru.ovechkin.tm.service.ProjectService;
import ru.ovechkin.tm.service.TaskService;
import ru.ovechkin.tm.util.TerminalUtil;

public final class Bootstrap {

    private final CommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ICommandController commandController = new CommandController(commandService);

    private final ITaskRepository taskRepository = new TaskRepository();

    private final ITaskService taskService = new TaskService(taskRepository);

    private final ITaskController taskController = new TaskController(taskService);

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final IProjectController projectController = new ProjectController(projectService);

    public void run(final String[] args) {
        commandController.displayWelcome();
        if (parseArgs(args)) System.exit(0);
        process();
    }

    private void process() {
        String command = "";
        while (!ICmdConst.CMD_EXIT.equals(command)) {
            System.out.print("Enter command: ");
            command = TerminalUtil.nextLine();
            parseCommand(command);
            parseArg(command);
            System.out.println();
        }
    }

    private boolean parseArgs(final String[] args) {
        if (args == null || args.length == 0) return false;
        final String arg = args[0];
        parseArg(arg);
        parseCommand(arg);
        return true;
    }

    private void parseArg(final String arg) {
        if (arg == null || arg.isEmpty()) return;
        switch (arg) {
            case IArgumentConst.ARG_VERSION:
                commandController.showVersion();
                break;
            case IArgumentConst.ARG_ABOUT:
                commandController.showAbout();
                break;
            case IArgumentConst.ARG_HELP:
                commandController.showHelp();
                break;
            case IArgumentConst.ARG_INFO:
                commandController.showInfo();
                break;
            case IArgumentConst.ARG_ARGUMENTS:
                commandController.showArguments();
                break;
            case IArgumentConst.ARG_COMMANDS:
                commandController.showCommands();
                break;
        }
    }

    private void parseCommand(final String arg) {
        if (arg == null || arg.isEmpty()) return;
        switch (arg) {
            case ICmdConst.CMD_VERSION:
                commandController.showVersion();
                break;
            case ICmdConst.CMD_ABOUT:
                commandController.showAbout();
                break;
            case ICmdConst.CMD_HELP:
                commandController.showHelp();
                break;
            case ICmdConst.CMD_INFO:
                commandController.showInfo();
                break;
            case ICmdConst.CMD_COMMANDS:
                commandController.showCommands();
                break;
            case ICmdConst.CMD_ARGUMENTS:
                commandController.showArguments();
                break;
            case ICmdConst.CMD_TASK_LIST:
                taskController.showTasks();
                break;
            case ICmdConst.CMD_TASK_CREATE:
                taskController.createTask();
                break;
            case ICmdConst.CMD_TASK_CLEAR:
                taskController.clearTasks();
                break;
            case ICmdConst.CMD_PROJECT_LIST:
                projectController.showProjects();
                break;
            case ICmdConst.CMD_PROJECT_CREATE:
                projectController.createProjects();
                break;
            case ICmdConst.CMD_PROJECT_CLEAR:
                projectController.clearProjects();
                break;
            case ICmdConst.CMD_EXIT:
                commandController.exit();
                break;
        }
    }

}
