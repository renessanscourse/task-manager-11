package ru.ovechkin.tm.repository;

//Репозиторий хранит данные о предметной области

import ru.ovechkin.tm.api.repository.ICommandRepository;
import ru.ovechkin.tm.constant.IArgumentConst;
import ru.ovechkin.tm.constant.ICmdConst;
import ru.ovechkin.tm.model.Command;

import java.util.Arrays;

public class CommandRepository implements ICommandRepository {

    public static final Command HELP = new Command(
            ICmdConst.CMD_HELP, IArgumentConst.ARG_HELP, " Display terminal commands"
    );

    public static final Command ABOUT = new Command(
            ICmdConst.CMD_ABOUT, IArgumentConst.ARG_ABOUT, " Show developer info"
    );

    public static final Command VERSION = new Command(
            ICmdConst.CMD_VERSION, IArgumentConst.ARG_VERSION, " Show version info"
    );

    public static final Command INFO = new Command(
            ICmdConst.CMD_INFO, IArgumentConst.ARG_INFO, " Display system's info"
    );

    public static final Command HELP_COMMANDS = new Command(
            ICmdConst.CMD_COMMANDS, IArgumentConst.ARG_COMMANDS, " Show available commands"
    );

    public static final Command HELP_ARGUMENTS = new Command(
            ICmdConst.CMD_ARGUMENTS, IArgumentConst.ARG_ARGUMENTS, " Show available arguments"
    );

    private static final Command TASK_CREATE = new Command(
            ICmdConst.CMD_TASK_CREATE, null, " Create new task"
    );

    private static final Command TASK_CLEAR = new Command(
            ICmdConst.CMD_TASK_CLEAR, null, " Remove all tasks"
    );

    private static final Command TASK_LIST = new Command(
            ICmdConst.CMD_TASK_LIST, null, " Show task list"
    );

    private static final Command PROJECT_CREATE = new Command(
            ICmdConst.CMD_PROJECT_CREATE, null, " Create new project"
    );

    private static final Command PROJECT_CLEAR = new Command(
            ICmdConst.CMD_PROJECT_CLEAR, null, " Remove all projects"
    );

    private static final Command PROJECT_LIST = new Command(
            ICmdConst.CMD_PROJECT_LIST, null, " Show project list"
    );

    public static final Command EXIT = new Command(
            ICmdConst.CMD_EXIT, null, " Close application"
    );

    private final Command[] TERMINAL_COMMANDS = new Command[]{
            HELP, ABOUT, VERSION, INFO, HELP_COMMANDS, HELP_ARGUMENTS,
            TASK_CREATE, TASK_LIST, TASK_CLEAR,
            PROJECT_CREATE, PROJECT_LIST, PROJECT_CLEAR,
            EXIT
    };

    private final String[] COMMANDS = getCommands(TERMINAL_COMMANDS);

    private final String[] ARGS = getArgs(TERMINAL_COMMANDS);

    public String[] getCommands(Command... values) {
        if (values == null || values.length == 0) return new String[]{};
        final String[] result = new String[values.length];
        int index = 0;
        for (int i = 0; i < values.length; i++) {
            final String name = values[i].getName();
            if (name == null || name.isEmpty()) continue;
            result[index] = name;
            index++;
        }
        return Arrays.copyOfRange(result, 0, index);
    }

    public String[] getArgs(Command... values) {
        if (values == null || values.length == 0) return new String[]{};
        final String[] result = new String[values.length];
        int index = 0;
        for (int i = 0; i < values.length; i++) {
            final String arg = values[i].getArg();
            if (arg == null || arg.isEmpty()) continue;
            result[index] = arg;
            index++;
        }
        return Arrays.copyOfRange(result, 0, index);
    }

    public Command[] getTerminalCommands() {
        return TERMINAL_COMMANDS;
    }

    public String[] getCommands() {
        return COMMANDS;
    }

    public String[] getArgs() {
        return ARGS;
    }

}
