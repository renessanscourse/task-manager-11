package ru.ovechkin.tm.api.controller;

public interface IProjectController {

    void showProjects();

    void clearProjects();

    void createProjects();

}
