package ru.ovechkin.tm.api.service;

import ru.ovechkin.tm.model.Command;

public interface ICommandService {

    Command[] getTerminalCommands();

    String[] getCommands();

    String[] getArgs();
}
