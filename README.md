# PROJECT INFO

**TASK-MANAGER**

# DEVELOPER INFO

**NAME:** Ovechkin Roman

**E-MAIL:** roman@ovechkin.ru

# SOFTWARE

- JDK 1.8
- MS WINDOWS 10

# PROGRAM RUN

```bash
java -jar ./task-manager.jar
```

# SCREENSHOTS

| Описание | Ссылка |
|:----|:----|
| Создание, вывод, удаление задач | https://yadi.sk/i/-oIwHWqiEwvNnQ | 
| Создание, вывод, удаление проектов | https://yadi.sk/d/4e4pZqxfsZG-fg  |